## Copying and Pasting in between the Tmux windows

**Fuente**: (https://linuxhint.com/copy-paste-clipboard-tmux/) 
- **Step 1**. Press the ‘Prefix’ <kbd>(Ctrl+b)</kbd> and then press <kbd>[</kbd> to enter the copy mode.

- **Step 2**. Using the arrow keys, locate the position to start copying from. Use the <kbd>Ctrl+spacebar</kbd> to start copying.

- **Step 3**. Move with the arrow keys to the position of the text you want to copy to. When you have finished selecting the text, press <kbd>Alt+w</kbd> or <kbd>Ctrl+w</kbd> to copy the text to a Tmux Buffer.

- **Step 4**. Paste the text to a Tmux pane/window/session using the Prefix (by default, it is <kbd>Ctrl+b</kbd> ) followed by <kbd>]</kbd>.
